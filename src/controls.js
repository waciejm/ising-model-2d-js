'use strict';
import { Color } from './color.js';

// The part that controls the simulation.

export class Controls {
    constructor() {
        this.temperature_label = document.getElementById("label_temperature_value");
        this.temperature_slider = document.getElementById("slider_temperature");

        this.field_label = document.getElementById("label_field_value");
        this.field_slider = document.getElementById("slider_field");

        this.manual_input_temperature = document.getElementById("field_temperature");
        this.manual_input_field = document.getElementById("field_field");
        this.manual_input_pressed = false;
        document
            .getElementById("button_manual_input")
            .addEventListener("click", () => {
                this.manual_input();
                this.manual_input_pressed = true;
            });

        this.step_pressed = false;
        document
            .getElementById("button_update")
            .addEventListener("click", () => this.step_pressed = true);
        this.autostep_checkbox = document.getElementById("checkbox_autostep");

        this.sweep_label = document.getElementById("label_lattice_sweep_value");
        this.sweep_slider = document.getElementById("slider_lattice_sweep");
        
        this.resolution_select = document.getElementById("select_lattice_size");

        this.color_positive_picker = document.getElementById("color_positive");
        this.color_negative_picker = document.getElementById("color_negative");

        document
            .getElementById("button_reset_parameters")
            .addEventListener("click", () => this.reset_controls());
        this.reset_pressed = false;
        document
            .getElementById("button_reset")
            .addEventListener("click", () => {
                // this.reset_controls();
                this.reset_pressed = true;
            });

        this.reset_controls();
    }

    update() {
        this.manual_input_pressed = false;
        this.reset_pressed = false;
        this.step_pressed = false;
        this.temperature_label.innerHTML = this.temperature().toFixed(4);
        this.field_label.innerHTML = this.field().toFixed(4);
        this.sweep_label.innerHTML = this.lattice_sweep().toFixed(4);
    }

    reset_controls() {
        this.temperature_slider.value = Math.sqrt(2.26918531421);
        this.field_slider.value = 0;
    }

    manual_input() {
        this.temperature_slider.value = Math.sqrt(this.manual_input_temperature.value);
        this.field_slider.value = Math.sqrt(this.manual_input_field.value);
        this.temperature_label.innerHTML = this.temperature().toFixed(4);
        this.field_label.innerHTML = this.field().toFixed(4);
    }

    // Simulation control

    canvas() {
        return document.getElementById("canvas");
    }

    magnetization() {
        return document.getElementById("label_magnetization");
    }

    resolution() {
        return Number(this.resolution_select.value);
    }

    reset() {
        return this.reset_pressed;
    }

    step() {
        return this.step_pressed || this.autostep_checkbox.checked;
    }

    lattice_sweep() {
        return Math.pow(this.sweep_slider.value, 5);
    }

    color_positive() {
        return parse_color(this.color_positive_picker.value);
    }

    color_negative() {
        return parse_color(this.color_negative_picker.value);
    }

    temperature() {
        return Math.pow(this.temperature_slider.value, 2);
    }

    field() {
        let field_pre_pow = this.field_slider.value;
        if (field_pre_pow >= 0) {
            return Math.pow(field_pre_pow, 2);
        } else {
            return -Math.pow(field_pre_pow, 2);
        }
    }
}

function parse_color(color) {
    let r = parse_channel(color[1] + color[2]);
    let g = parse_channel(color[3] + color[4]);
    let b = parse_channel(color[5] + color[6]);
    return new Color(r, g, b);
}

function parse_channel(channel) {
    return parse_letter(channel[0]) * 16 + parse_letter(channel[1]);
}

function parse_letter(letter) {
    if (letter == '0') {
        return 0;
    } else if (letter == '1') {
        return 1;
    } else if (letter == '2') {
        return 2;
    } else if (letter == '3') {
        return 3;
    } else if (letter == '4') {
        return 4;
    } else if (letter == '5') {
        return 5;
    } else if (letter == '6') {
        return 6;
    } else if (letter == '7') {
        return 7;
    } else if (letter == '8') {
        return 8;
    } else if (letter == '9') {
        return 9;
    } else if (letter == 'a' || letter == 'A') {
        return 10;
    } else if (letter == 'b' || letter == 'B') {
        return 11;
    } else if (letter == 'c' || letter == 'C') {
        return 12;
    } else if (letter == 'd' || letter == 'D') {
        return 13;
    } else if (letter == 'e' || letter == 'E') {
        return 14;
    } else if (letter == 'f' || letter == 'F') {
        return 15;
    }
}