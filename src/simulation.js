'use strict';
import { Color } from './color.js';

// The part that simulates and draws the simulation.

// Simulation requires a controls struct to control simulation.
// Functions required to be implemented on controls object listed at the bottom.
export class Simulation {
    constructor(controls) {
        let canvas = controls.canvas(); // Canvas for drawing the simulation.
        this.ctx = canvas.getContext("2d"); // Canvas context.
        this.magnetization = controls.magnetization(); // HTML element to write magnetization to.
        this.width = canvas.width; // Width of the simulation canvas.
        this.height = canvas.height; // Height of the simulation canvas.
        this.resolution; // Number of cells in one dimension.
        this.cells; // A 2D array of cells.
        this.cell_width; // Width of a cell as drawn in pixels.
        this.cell_height; // Height of a cell as drawn in pixels.
        this.step = false; // Whether to take a step during update.
        this.steps_per_frame = 1; // Nummber of steps per update.
        this.color_positive; // Color of a cell with positive value.
        this.color_negative; // Color of a cell with negative value.
        this.color_error = new Color(255, 128, 128); // Color of a cell with an unexpected (wrong) value for error detecting.
        this.temperature; // Temperature of the simulation.
        this.field; // External field

        this.update_settings(controls);
    }

    update() {
        if (this.step) {
            let steps = Math.floor(this.lattice_sweep * Math.pow(this.resolution, 2));
            for (let step = 0; step < steps; ++step) {
                let x = Math.floor(Math.random() * this.resolution);
                let y = Math.floor(Math.random() * this.resolution);
                this.simulation_step(x, y);
            }
            this.magnetization.innerHTML = this.calculate_magnetization().toFixed(4);
        }
    }

    simulation_step(x, y) {
        let de = calculate_de(
            this.cells[x][y],
            x+1 < this.resolution ? this.cells[x+1][y] : 0,
            x-1 >= 0 ? this.cells[x-1][y] : 0,
            y+1 < this.resolution ? this.cells[x][y+1] : 0,
            y-1 >= 0 ? this.cells[x][y-1] : 0,
            this.field,
        );
        if (de <= 0) {
            this.cells[x][y] *= -1;
        } else {
            let r = Math.random();
            let expBE = Math.exp(-de / this.temperature);
            if (expBE > r) {
                this.cells[x][y] *= -1;
            }
        }
    }

    calculate_magnetization() {
        let magnetization = 0;
        for (let x = 0; x < this.resolution; ++x) {
            for (let y = 0; y < this.resolution; ++y) {
                magnetization += this.cells[x][y];
            }
        }
        return magnetization / Math.pow(this.resolution, 2);
    }

    update_settings(controls) {
        let new_resolution = controls.resolution();
        if (
            new_resolution != this.resolution ||
            controls.reset()
        ) {
            this.rebuild(controls);
        }
        this.step = controls.step();
        this.lattice_sweep = controls.lattice_sweep();
        this.color_positive = controls.color_positive();
        this.color_negative = controls.color_negative();
        this.temperature = controls.temperature();
        this.field = controls.field();
    }

    rebuild(controls) {
        this.resolution = controls.resolution();
        this.cells = create_cells_fn(
            this.resolution,
            (x, y) => {
                let val;
                if (Math.random() >= 0.5) {
                    val = 1;
                } else {
                    val = -1;
                };
                return val;
            },
        );
        this.cell_width = this.width / this.resolution;
        this.cell_height = this.height / this.resolution;
    }

    draw() {
        let image_data = this.ctx.getImageData(0, 0, this.width, this.height);
        let img_width = this.resolution * this.cell_width;
        let img_height = this.resolution * this.cell_height;
        for (let x = 0; x < this.resolution; ++x) {
            for (let w = 0; w < this.cell_width; ++w) {
                for (let y = 0; y < this.resolution; ++y) {
                    for (let h = 0; h < this.cell_height; ++h) {
                        let index = 4 * (
                            img_width * (this.cell_height * y + h)
                            + (this.cell_width * x) + w
                        );
                        let color = this.get_color(this.cells[x][y]);
                        image_data.data[index + 0] = color.r;
                        image_data.data[index + 1] = color.g;
                        image_data.data[index + 2] = color.b;
                        image_data.data[index + 3] = 255;
                    }
                }
            }
        }
        this.ctx.putImageData(image_data, 0, 0);
    }

    get_color(value) {
        let positive = 1;
        let negative = -1;
        if (value == positive) {
            return this.color_positive;
        } else if (value == negative) {
            return this.color_negative;
        } else {
            return this.color_error;
        }
    }
}

// Creates a square cell lattice of given size
// with the value of a cell determined by given function fn(x, y)
function create_cells_fn(resolution, fn) {
    let cells = new Array();
    for (let x = 0; x < resolution; ++x) {
        let array = new Array();
        for (let y = 0; y < resolution; ++y) {
            array.push(fn(x, y));
        }
        cells.push(array);
    }
    return cells;
}

function calculate_de(cell, n1, n2, n3, n4, field) {
    return -2 * (-cell * (n1 + n2 + n3 + n4) - cell * field);
}


// Functions required to be implemented on controls object:

// canvas() -> Canvas
// The canvas element to draw lattice on.

// magnetization() -> Label
// HTML element to write magnetization to.

// resolution() -> int
// The amount of cells in one dimension of simulation.
// (side of a rectangle)

// reset() -> bool
// Whether to reset the simulation in next update.

// step() -> bool
// Whether to take steps in next update.

// lattice_sweep() -> float
// How many steps to take per update (lattice_sweep * resolution^2).

// color_positive() -> Color
// The color in which to draw positive cells.

// color_negative() -> Color
// The color in which to draw negative cells.

// temperature() -> float
// Temperature of the simulation.

// field() -> float
// External field
