'use strict';

import { Simulation } from './simulation.js';
import { Controls } from './controls.js';

let controls = new Controls();
let simulation = new Simulation(controls);

let simulation_loop = () => {
    simulation.update_settings(controls);
    simulation.update();
    simulation.draw();
    controls.update();
    window.requestAnimationFrame(simulation_loop);
};

simulation_loop();