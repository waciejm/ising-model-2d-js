export class Color {
    constructor(r, g, b, a) {
        this.r = (r != null) ? r : 255;
        this.g = (g != null) ? g : 255;
        this.b = (b != null) ? b : 255;
        this.a = (a != null) ? a : 255;
    }
}